/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bigcornshow.model;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;

/**
 *
 * @author lucas.nunes
 */
public class Tables extends AbstractTableModel {

    private ArrayList lines     = null;
    private String[] columns    = null;
    
    /**
     *
     * @param line 
     * @param column 
     */
    public Tables(ArrayList line,String[] column){
        setLines(line);
        setColumns(column);
    }
    
    /**
     *
     * @return
     */
    public ArrayList getLines(){
        return lines;
    }
    
    /**
     *
     * @param dados
     */
    public void setLines(ArrayList dados){
        lines = dados;
    }

    /**
     *
     * @return
     */
    public String[] getColumns (){
        return columns;
    }

    /**
     *
     * @param nomes
     */
    public void setColumns(String[] nomes){
        columns = nomes;
    }
    
    /**
     *
     * @return
     */
    @Override
    public int getColumnCount(){
        return columns.length;       
    }
    
    /**
     *
     * @return
     */
    @Override
    public int getRowCount(){
        return lines.size();
    }
    
    /**
     *
     * @param numCol
     * @return
     */
    @Override
    public String getColumnName(int numCol){
        return columns[numCol];
    }

    /**
     *
     * @param numLin
     * @param numCol
     * @return
     */
    @Override
    public Object getValueAt(int numLin, int numCol){
        Object[] linha = (Object[])getLines().get(numLin);
        return linha[numCol];
    }
    
}
