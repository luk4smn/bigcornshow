/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bigcornshow.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Lucas
 */
public class DatabaseManager {

    public Statement stm; //prepara e realiza pesquisa no BD
    public ResultSet rs; //armazena o resultado da pesquisa
    private String driver,path,dbUser,dbPassword;
    
    
private static final Properties config = new Properties();
private static final String file = "config.ini";

  /**
     *
     */
    public Connection connection; //conecta o BD
    
    /**
     * Abre conexao cm o BD
     */
    public void connect(){
        try {
            try {
            //tentativa inicial de conexão
            config.load(new FileInputStream(file));
            driver = config.getProperty("driver");
            path = config.getProperty("path");
            dbUser = config.getProperty("db_user");
            dbPassword = config.getProperty("db_pass");
            
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null,"ARQUIVO DE CONFIGURAÇÃO NÃO ENCONTRADO \n" +ex);
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.setProperty("jdbc.Drivers", driver);                             //seta a propriedade do driver de conexão
            Class.forName(driver); 
            connection = DriverManager.getConnection(path, dbUser, dbPassword);    //realiza a conexão cm o BD
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"ERRO AO ABRIR CONEXÃO \n" +ex);
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            disconnect();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Executa Query
     * @param sql
     */
    public void runSQL (String sql){
        try{
            stm = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stm.executeQuery(sql);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog (null, "Erro de Execução SQL!\n Erro" +ex.getMessage());
        }
    }
    
    /**
     * Fecha a conexao do BD
     */
    public void disconnect(){
        try {
          connection.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"ERRO AO FECHAR CONEXÃO");
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
}
