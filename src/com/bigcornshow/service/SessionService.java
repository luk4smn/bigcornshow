/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bigcornshow.service;

import com.bigcornshow.database.DatabaseManager;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 *
 * @author lucas.nunes
 */
public class SessionService {
    
    private static SessionService single_instance = null;
   
    DatabaseManager database = new DatabaseManager();
     Clip clip = null;
    
     public static SessionService getInstance(){ 
        if (single_instance == null) 
            single_instance = new SessionService(); 
        
        return single_instance; 
    }
    
    public void playSound(String soundFile) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
       AudioInputStream audioIn = null;
        try {
            File f = new File("" + soundFile);
            System.out.println(""+f.toURI().toURL());
            audioIn = AudioSystem.getAudioInputStream(f.toURI().toURL());
            clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        } catch (MalformedURLException ex) {
            Logger.getLogger(SessionService.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public void stopSound() {
        clip.stop();
    }
    
   
    
}
